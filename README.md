PTutil
======

A collection of utilities for Pluggable Transports.

SafeLog
-------

A safer logging wrapper around the standard logging package. It removes private parts from the logs, like IP addresses.

```go
log.SetOutput(&safelog.LogScrubber{Output: os.Stderr})
```

SafeProm
--------

Implements some additional prometheus metrics that we need for privacy preserving
counts of users and proxies.

```go
usersTotal = safeprom.NewRoundedCounterVec(
    prometheus.CounterOpts{
        Name:      "rounded_users_total",
        Help:      "The number of users, rounded up to a multiple of 8",
    },
    []string{"status"},
)
usersTotal.WithLabelValues("active").Inc()
```
